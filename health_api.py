#!/usr/bin/python
#
# HEALTH CLUSTER CHECK FOR ELASTICSEARCH DEPLOYMENT
# LICENSE: GNU gpl-v3 (SEE LICENSE File)
# USAGE: python health_api.py -c config 
#         -d for debug
#         -h for help
#         -p udp syslog port ( python health_api.py -c config 127.0.0.1 -p 514 )
#
#TODO: 1. Syslog messages instead of only email messages
#      2. Custom/peronalized Email Body
#      3. HTTP Connecntion API check 
#      4. more o-o flexible code
#      5. stats for Kibana integration UDP Message for Logstash or Pre-build json POST
#              - Ram usage during the week
#              - CPU usage during the week
#              - Document indexed during the week
#              - Disk usage during the week
#      6. Add Option to disbable/enable ElasticSearch Auth
#

import json
import requests
import ConfigParser, sys, os,time
# Import smtplib for the actual sending function
import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText
from optparse import OptionParser
from requests.auth import HTTPBasicAuth

# Config VAR
VERSION = "2.1b"

#Config parser
Config = ConfigParser.ConfigParser()
SECTIONS = ['PROXY','EMAIL','GENERAL','CLUSTER','OS','FS'] #Keep ordering
USAGE = """
TODO: palce here custom config with better explanation of his fields
See config.cfg.example for more info
"""
parser = OptionParser(USAGE)
parser.add_option("-c",
                  action="store", type="string", dest="config",
                  default = "",
                  help="load configuration file")
parser.add_option("-v",
                  action="store_true", dest="version",
                  default=False,
                  help="show version")
parser.add_option("-d",
                  action="store_true", dest="debug",
                  default=False,
                  help="show debug information")
parser.add_option("-p",
                  action="store", dest="port",
                  default=False,
                  help="specify custom udp destination port")

(options, args) = parser.parse_args()

if options.version:
    print "ELK Health Check", VERSION
    sys.exit()

if options.port:
    dport = options.port
    dport = int(dport)
else:
    dport = 514
    
if (len(args) < 0):
    parser.print_help()
    sys.exit()
else:
	if(len(args) > 1):
         dest = args[0]
#Default configuration
CONFIG={}
CONFIG['FACILITY'] = {
        'kern': 0, 'user': 1, 'mail': 2, 'daemon': 3,
        'auth': 4, 'syslog': 5, 'lpr': 6, 'news': 7,
        'uucp': 8, 'cron': 9, 'authpriv': 10, 'ftp': 11,
        'local0': 16, 'local1': 17, 'local2': 18, 'local3': 19,
        'local4': 20, 'local5': 21, 'local6': 22, 'local7': 23,
}

CONFIG['LEVEL'] = {
        'emerg': 0, 'alert':1, 'crit': 2, 'err': 3,
        'warning': 4, 'notice': 5, 'info': 6, 'debug': 7
}


# read options from config ( -c filename )
if options.config != "":
    if os.access(options.config,os.R_OK):
        Config.read(options.config)
        
        for section in Config.sections():
            if options.debug:
                print "Parsing", section
            if section not in SECTIONS:
                continue
            
            if section == SECTIONS[3]:
                CONFIG[section] = []
                for option,value in Config.items(section):
                    if options.debug:
                        print "    ", value
                    CONFIG[section].append(value)
            else:
                # Special handling for descriptive config value
                CONFIG[section]={}
                for option,value in Config.items(section):
                    if options.debug:
                        print "    %s = %s" % (option,value)
                    CONFIG[section][option]=value
    else:
        print "Unable to load configuration file:", options.config
        print "Using default configuration"


# json cluster array data - body_type ( cluster_status or node_status)- subject - to - from 
# send email function
# TODO: more flexible code and add custom body personalization!
def send_email(data,body_type,to,from_mail):
     if body_type == "cluster_status":
          msg = MIMEText(json.dumps(data, indent=4))
     #print data
          if 'subject' not in globals():
               msg['Subject'] = CONFIG[SECTIONS[1]]['subject_header']+' Cluster '+data['cluster_name']+' State '+data['status']
          else:
               msg['Subject'] = CONFIG[SECTIONS[1]]['subject_header']+' '+subject
     else:
          if body_type == "node_status":
               msg = MIMEText(json.dumps(data, indent=4))
               if 'subject' not in globals():
                    msg['Subject'] = CONFIG[SECTIONS[1]]['subject_header']+' Cluster '+data['cluster_name']
               else:
                    msg['Subject'] = CONFIG[SECTIONS[1]]['subject_header']+'  '+subject
          else:
              msg = MIMEMultipart()
     msg['From'] = from_mail
     
     if ',' in to:
          receiver = to.split(',')
          msg['To'] = ', '.join( receiver )
     else:
          msg['To'] = to
          receiver = to

     if options.debug:
          print msg.as_string()
     else:
	# me == the sender's email address
	# you == the recipient's email address 
	# Send the message via our own SMTP server, but don't include the
	# envelope header.
          s = smtplib.SMTP(CONFIG[SECTIONS[1]]['smtp'])
          s.sendmail(from_mail, receiver, msg.as_string())
          s.quit()
          
# TODO: send sylog messages if alarm fires
def send_syslog(message, level=CONFIG['LEVEL']['notice'], facility=CONFIG['FACILITY']['daemon'], host='localhost', port=512):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        data = '<%d>%s' % (level + facility*8, message)
        sock.sendto(data, (host, port))
        sock.close()

# throttling alerts so it dont fire too much [cluster name: ip for read config file , alert_type: swap ram fs os load ecc]
def save_throttling(cluster_name,alert_type):
     config_values = CONFIG['GENERAL']['throttling'].split(':')
     if config_values[0] != 0:
          if os.path.exists('./'+cluster_name):
               if options.debug:
                    print 'save throttling'
               data = json.load(open(cluster_name))
               if alert_type in data:
                    data[alert_type] = int(data[alert_type]) + 1
               else:
                    data[alert_type] = 1
               # add sleep time amount to the correct alert so throttling can increase the amount of time to sleep
               # it is consider only the first slepp time configured.
               if data['first_alarm'] == '0':
                    data['second_alamr'] = int(data['second_alarm']) + int(CONFIG[SECTIONS[2]]['sleep_alert'].split(':')[0])
               else:
                    data['first_alarm'] = int(data['first_alarm']) + int(CONFIG[SECTIONS[2]]['sleep_alert'].split(':')[0])                    
               json.dump(data, open('./'+cluster_name,'w'))

# return 1 if alert need to be throttled 0 if alert can fire however
# TODO: different throttling for medium and critical alert config_values[0] is throttling for medium alert
def throttling(cluster_name,alert_type):
     config_values = CONFIG['GENERAL']['throttling'].split(':')
     if os.path.exists('./'+cluster_name):
          data = json.load(open(cluster_name))
          # skip the first run of scripts so the key isent already in json cluster file
          if not alert_type in data:
               return 0
          if int(data[alert_type]) < int(config_values[0]):
               save_throttling(cluster_name,alert_type)
               return 1
          else:
               data[alert_type] = 0
               json.dump(data, open('./'+cluster_name,'w'))
               
# save a json with IP:PORT filename that prevent alert to fired every cronjob activity on this Scripts!
def save_AlertToSleep(cluster_name):
     if os.path.exists('./'+cluster_name):
          if options.debug:
               print 'saveAlertToSpleep'
          data = json.load(open(cluster_name))
          if int(data['second_alarm']) > int(data['first_alarm']):
#               d = {"first_alarm":int(str(time.time()).split('.')[0]),"second_alarm":0}
               data['second_alarm'] = 0
               data['first_alarm'] = int(str(time.time()).split('.')[0])
          else:
          # prevent to fire second alarm for node in same Cluster this usually happen when both nodes fired an alarm on the same scripts exectuion
          # the second timestamp need to be write only after first sleep time, usually 7200 placed on config.cfg under first part of sleep_alarm settings
               if int(str(time.time()).split('.')[0]) - 30 > int(data['first_alarm']):
#                    d = {"first_alarm":data['first_alarm'],"second_alarm":int(str(time.time()).split('.')[0])}
                    data['first_alarm'] = int(str(time.time()).split('.')[0])
               else:
#                    d = {"first_alarm":int(str(time.time()).split('.')[0]),"second_alarm":0}
                    data['second_alarm'] = 0
                    data['first_alarm'] = int(str(time.time()).split('.')[0])
     else:
          data = {"first_alarm":int(str(time.time()).split('.')[0]),"second_alarm":0}
     json.dump(data, open('./'+cluster_name,'w'))

# Check if alarm can fire against sleep options placed in the config
def alarm_CanFire(cluster_name):
     if os.path.exists('./'+cluster_name):
          if options.debug:
               print 'can_fire?'+cluster_name
          data = json.load(open(cluster_name))
          if int(data['second_alarm']) > int(data['first_alarm']):
               # current time - sleep time configured in config file > time of second alarm fired
               if int(str(time.time()).split('.')[0])-int(CONFIG[SECTIONS[2]]['sleep_alert'].split(':')[1]) > int(data['second_alarm']):
                    return 1
               else:
                    return 0
          else:
               # current time - sleep time configured in config file > time of first alarm fired
               if int(str(time.time()).split('.')[0])-int(CONFIG[SECTIONS[2]]['sleep_alert'].split(':')[0]) > int(data['first_alarm']):
                    return 1
               else:
                    return 0
     else:
          return 1                  

# Choose what type of alert fire: send an email or trigger a syslog message
def alert(json_data,body_type):
     # Email is enabled ?
	if CONFIG[SECTIONS[1]]['email'] == "True":
	     # body to send, TO Address, FROM Address
		send_email(json_data,body_type,CONFIG[SECTIONS[1]]['to_mail'],CONFIG[SECTIONS[1]]['from_mail'])
		save_AlertToSleep(string_ip[:-1][7:])
	if CONFIG[SECTIONS[2]]['syslog'] == "False" and 'dest' in globals():
		print 'undefined syslog'

# check for cluster status Green - Yellow - Red 
# then send an email if debug mode is true every time 
# also when cluster is green with json in the email body		
def check_status(json_data,body_type):
     if CONFIG[SECTIONS[1]]['email_facility'] == "debug":
          alert(json_data,body_type)
     if CONFIG[SECTIONS[1]]['email_facility'] == "error":
          if json_data['status'] != "green":
               alert(json_data,body_type)
     if json_data['status'] == "red" and CONFIG[SECTIONS[1]]['email_facility'] == "critical":
          alert(json_data,body_type)

# check RAM usage on specific Node HIGH or Medium Alert 
# caused by config value [OS] mem
def check_ram(json_data):
     config_options = CONFIG[SECTIONS[4]]['mem'].split(':')
    # if options.debug:
    #     print json_data
     # alert HIGH severity
     if options.debug:
          print os_stats['nodes'][nodes_id]['name']+" ACTUAL FREE % RAM: "+str(os_stats['nodes'][nodes_id]['os']['mem'][config_options[0]])+" FIRE AT High: "+config_options[1]+"%"+" Medium: "+config_options[2]+"%" 
     if int(os_stats['nodes'][nodes_id]['os']['mem'][config_options[0]]) <= int(config_options[1]):
          return 'RAM USAGE CRITICAL on '+os_stats['nodes'][nodes_id]['name']+': '+str(os_stats['nodes'][nodes_id]['os']['mem'][config_options[0]])+'% free'
     else:
     # Alert Medium
          if int(os_stats['nodes'][nodes_id]['os']['mem'][config_options[0]]) <= int(config_options[2]):
               return 'RAM USAGE MEDIUM on '+os_stats['nodes'][nodes_id]['name']+': '+str(os_stats['nodes'][nodes_id]['os']['mem'][config_options[0]])+'% free'
          else:
               return 0
               
# check SWAP usage on specific Node HIGH or Medium Alert 
# caused by config value [OS] swap
def check_swap(json_data):
     config_options = CONFIG[SECTIONS[4]]['swap'].split(':')
     if options.debug:
          print os_stats['nodes'][nodes_id]['name']+" ACTUAL USED SWAP: "+str(os_stats['nodes'][nodes_id]['os']['swap'][config_options[0]])+" FIRE AT High: "+config_options[1]+"mb"+" Medium: "+config_options[2]+"mb"      
     if '0b' in os_stats['nodes'][nodes_id]['os']['swap'][config_options[0]]:
          return 0
     # alert HIGH severity
     if 'gb' in os_stats['nodes'][nodes_id]['os']['swap'][config_options[0]]:
          current_swap = int(float(os_stats['nodes'][nodes_id]['os']['swap'][config_options[0]][:-2]))*1000
     else:
          if not os_stats['nodes'][nodes_id]['os']['swap'][config_options[0]][:-4]:
              return 0
          current_swap = int(os_stats['nodes'][nodes_id]['os']['swap'][config_options[0]][:-4])
     if current_swap >= int(config_options[1]):
          return 'SWAP USAGE CRITICAL on '+os_stats['nodes'][nodes_id]['name']+': '+str(os_stats['nodes'][nodes_id]['os']['swap'][config_options[0]])
     else:
     # Alert Medium
          if current_swap >= int(config_options[2]):
               return 'SWAP USAGE MEDIUM on '+os_stats['nodes'][nodes_id]['name']+': '+str(os_stats['nodes'][nodes_id]['os']['swap'][config_options[0]])
          else:
               return 0
               
# check Load usage on specific Node HIGH or Medium Alert 
# caused by config value [OS] load_avarage
def check_load(json_data):
     config_options = CONFIG[SECTIONS[4]]['os'].split(':')
     if options.debug:
          print os_stats['nodes'][nodes_id]['name']+" ACTUAL LOAD: "+str(os_stats['nodes'][nodes_id]['os']['load_average'][0])+" FIRE AT High: "+config_options[1]+" "+" Medium: "+config_options[2]+" "      
     # alert HIGH severity)
     if os_stats['nodes'][nodes_id]['os']['load_average'][0] >= float(config_options[1]):
          return 'Load USAGE CRITICAL on '+os_stats['nodes'][nodes_id]['name']+': '+str(os_stats['nodes'][nodes_id]['os']['load_average'][0])
     else:
     # Alert Medium
          if os_stats['nodes'][nodes_id]['os']['load_average'][1] >= float(config_options[2]):
               return 'Load USAGE MEDIUM on '+os_stats['nodes'][nodes_id]['name']+': '+str(os_stats['nodes'][nodes_id]['os']['load_average'][0])
          else:
               return 0

# Check DISK USAGE on Specific Node , HIGH or Medium Alert is possible
# caused by config value [FS] total and [FS] data
# TODO: improve multi path check!!!
def check_disk():
     config_options = CONFIG[SECTIONS[5]]['total'].split(':')
     #print fs_stats['nodes'][nodes_id]     
     if 'total' in fs_stats['nodes'][nodes_id]['fs']:
     # disk size value need to be normalized under same units ( free disk space space under total json node of _nodes/stats)
          if 'gb' in fs_stats['nodes'][nodes_id]['fs']['total'][config_options[0]]:
          # i'll take only the first number of Gb for ex: 43.2gb become 43gb thats all!
               current_disk_size = int(fs_stats['nodes'][nodes_id]['fs']['total'][config_options[0]][:-2].split('.')[0])
          else:
          # so if there isent Gb in the disk size that means that we are under the 1 gb disk size so it could be consider a 0 disk size!!
               current_disk_size = 0
          # alert CRITICAL if current free disk space is less then what we setup in config.cfg under [FS] total = free:?
          if options.debug:
               print os_stats['nodes'][nodes_id]['name']+" ACTUAL FREE DISK: "+str(current_disk_size)+"gb FIRE AT High: "+config_options[1]+"gb"+" Medium: "+config_options[2]+"gb"          
          if current_disk_size <= int(config_options[1]):
               return 'Free Disk Space CRITICAL on '+fs_stats['nodes'][nodes_id]['name']+': '+str(current_disk_size) + 'Gb'
          else:
          # Alert Medium
               if current_disk_size <= int(config_options[2]):
                    return 'Free Disk Space MEDIUM on '+fs_stats['nodes'][nodes_id]['name']+': '+str(current_disk_size) + 'Gb'
               else:
                    return 0
     else:
          # parse [FS] data = available:? config options Needed for > .90.7 ES version
          # TODO: loop into all data path!!
          config_options_data = CONFIG[SECTIONS[5]]['data'].split(':')
          # we loop into data patterns configured in ES config , we looking for some specific data directory full [FS] data = available:?!
          for data_path in fs_stats['nodes'][nodes_id]['fs']['data']:
               print data_path
               if 'gb' in data_path[config_options_data[0]]:
          # i'll take only the first number of Gb for ex: 43.2gb become 43gb thats all!
                    current_disk_size = int(data_path[config_options_data[0]][:-2].split('.')[0])
               else:
          # so if there isent Gb in the disk size that means that we are under the 1 gb disk size so it could be consider a 0 disk size!!
                    current_disk_size = 0
               if options.debug:
                    print os_stats['nodes'][nodes_id]['name']+" ACTUAL AVAILABLE ON PATH: "+str(current_disk_size)+"gb FIRE AT High: "+config_options_data[1]+"gb"+" Medium: "+config_options_data[2]+"gb"    
          # alert CRITICAL if current free disk space is less then what we setup in config.cfg under [FS] total = free:?
               if current_disk_size <= int(config_options_data[1]):
                    return 'Free Disk Space CRITICAL on '+fs_stats['nodes'][nodes_id]['name']+': '+str(current_disk_size) + 'Gb'
               else:
          # Alert Medium
                    if current_disk_size <= int(config_options_data[2]):
                         return 'Free Disk Space MEDIUM on '+fs_stats['nodes'][nodes_id]['name']+': '+str(current_disk_size) + 'Gb'
                    else:
                         return 0               

def check_ping(proxy):
     try:
          r = requests.get(string_ip,proxies=proxy)
     except requests.exceptions.Timeout:
    # Maybe set up for a retry, or continue in a retry loop
          return 'Host Down CRITICAL '+string_ip
     except requests.exceptions.ConnectionError:
    # Maybe set up for a retry, or continue in a retry loop
          return 'Host Down CRITICAL '+string_ip          
     except requests.exceptions.RequestException as e:
    # catastrophic error. bail.
          print e
          return 'Host Unknown Status CRITICAL '+string_ip
     del r
     return 1

# Override Proxy
if "proxy" in CONFIG['GENERAL'] and CONFIG['GENERAL']['proxy'] == "none":
    proxies = {
        'no': 'pass',
    }
elif "proxy" in CONFIG['GENERAL']:
    proxies = {
        'http': CONFIG['GENERAL']['proxy']
    }
else:
    proxies = {
        'no': 'pass',
    }


for string_ip in CONFIG['CLUSTER']:
     if 'user' in string_ip or 'password' in string_ip:
          continue
     # Override ENV variables because requests use proxy in UNIX ENV
     elastic_ip = string_ip.split("//")[1]
     if "/" in elastic_ip:
        elastic_ip = elastic_ip[:-1]
     os.environ['no_proxy'] = '127.0.0.1,localhost,'+elastic_ip
     # check if Alarm con fire or it is in sleep period 
     if not alarm_CanFire(string_ip[:-1][7:]):
          if options.debug:
               print "alarm can't fire?:"+string_ip[:-1][7:]
          continue    
     # if ping response give timeout skip all check           
     ping_alert = check_ping(proxies)
     if ping_alert != 1:
          subject = ping_alert
          alert('',"node_status")
          del subject
          continue
     if "none" in CONFIG['GENERAL']['username'] or CONFIG['GENERAL']['username'] == "":
        try:
            stats = requests.get(string_ip+'_cluster/health?pretty',proxies=proxies).json()
        except Exception as inst:
            subject = "Critical Error on get request on: "+string_ip
            alert(' ',"node_status")
            del subject
            continue
     else:
        try:
            stats = requests.get(string_ip+'_cluster/health?pretty',auth=(CONFIG['GENERAL']['username'], CONFIG['GENERAL']['password']),proxies=proxies).json()
        except Exception as inst:
             subject = "Critical Error on get request on: "+string_ip
             alert(' ',"node_status")
             del subject
             continue
     # check for cluster status GREEN - YELLOW - RED
     check_status(stats,"cluster_status")
     #print requests.get(string_ip+'_cluster/health?pretty',auth=(CONFIG[SECTIONS[1]]['user'], CONFIG[SECTIONS[1]]['password']))
     # HEALTH API ES 1.0
     if "none" in CONFIG['GENERAL']['username'] or not CONFIG['GENERAL']['username']:
        os_stats = requests.get(string_ip+'_nodes/stats/os?pretty&human=true',proxies=proxies).json()
        fs_stats = requests.get(string_ip+'_nodes/stats/fs?pretty&human=true',proxies=proxies).json()
     else:
        os_stats = requests.get(string_ip+'_nodes/stats/os?pretty&human=true',auth=(CONFIG['GENERAL']['username'],CONFIG['GENERAL']['password']),proxies=proxies).json()
        fs_stats = requests.get(string_ip+'_nodes/stats/fs?pretty&human=true',auth=(CONFIG['GENERAL']['username'], CONFIG['GENERAL']['password']),proxies=proxies).json()
     if stats['status'] == "green" and options.debug:
	     print "all is ok for "+stats['cluster_name']
     for nodes_id in os_stats['nodes']:
          if os_stats['nodes'][nodes_id]['name'] in CONFIG[SECTIONS[1]]['node_whitelist'].split(','):
               if options.debug:
                    print "Node To WHITELIST: "+os_stats['nodes'][nodes_id]['name']
               continue
	     #print str(os_stats['nodes'][nodes_id]['os']['load_average'][0]) + ' IP: ' + os_stats['nodes'][nodes_id]['transport_address']
          ram_alert = check_ram(os_stats)
          swap_alert = check_swap(os_stats)
          load_alert = check_load(os_stats)
          fs_alert = check_disk()
          if fs_alert != 0:
               if not throttling(string_ip[:-1][7:],"fs_alert"):
                    subject = fs_alert
                    alert(fs_stats,"node_status")
                    save_throttling(string_ip[:-1][7:],"fs_alert")
                    del subject
               else:
                    if options.debug:
                         print 'throttling fs_alert for ' +  string_ip[:-1][7:]       
          if ram_alert != 0 and fs_alert == 0:
               if not throttling(string_ip[:-1][7:],"ram_alert"):
                    subject = ram_alert
                    string_ip[:-1][7:],"fs_alert"
                    alert(os_stats,"node_status")
                    save_throttling(string_ip[:-1][7:],"ram_alert")
                    del subject
               else:
                    if options.debug:
                         print 'throttling ram_alert for ' +  string_ip[:-1][7:]                     
          if swap_alert != 0 and ram_alert == 0:
               if not throttling(string_ip[:-1][7:],"swap_alert"):
                    subject = swap_alert
                    alert(os_stats,"node_status")
                    save_throttling(string_ip[:-1][7:],"swap_alert")
                    del subject
               else:
                    if options.debug:
                         print 'throttling swap_alert for ' +  string_ip[:-1][7:]                     
          if load_alert != 0 and fs_alert == 0:
               if not throttling(string_ip[:-1][7:],"load_alert"):
                    subject = load_alert
                    alert(os_stats,"node_status")
                    save_throttling(string_ip[:-1][7:],"laod_alert")
                    del subject
               else:
                    if options.debug:
                         print 'throttling laod_alert for ' +  string_ip[:-1][7:]                     
     # check for cluster status GREEN - YELLOW - RED  
     #check_status(stats,"cluster_status")


